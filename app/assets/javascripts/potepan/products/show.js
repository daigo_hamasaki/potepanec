$(document).ready(function() {
  // ロード時にヴァリアントを選択済みであれば内容を表示
  if ($('select#variants').val() !== undefined) {
    loadVariantData();
  }

  // ヴァリアント選択時
  $('select#variants').change(function() {
    loadVariantData();
  });

  // 選択中のヴァリアントの内容を表示
  function loadVariantData() {
    var variant_id = $('select#variants').val();
    showPrices(variant_id);
    showImagesInCarousel(variant_id);    
  }

  // ヴァリアントの価格を表示
  function showPrices(variant_id) {
    $('#prices').find('span').each(function() {
      if ($(this).attr('id') == 'variant_prices_' + variant_id) {
        $(this).show();
      } else if ($(this).attr('id') == 'product_prices' && variant_id == 0) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  };

  // 商品画像とサムネイルの表示をヴァリアントに切り替え
  function showImagesInCarousel(variant_id) {
    // 商品画像カルーセル内のimgタグの要素を書き換え
    $('#image-carousel').find('img').each(function(i) {
      // 選択されたヴァリアントの画像を表示、それ以外の画像を非表示
      if ($(this).attr('id') == 'variant_image_' + variant_id) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });

    // サムネイルカルーセル内のimgタグの要素を書き換え
    $('#thumbnail-carousel').find('img').each(function() {
      // 選択されたヴァリアントのサムネイルを表示、それ以外のサムネイルを非表示
      if ($(this).attr('id') == 'variant_thumb_' + variant_id) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  };
});