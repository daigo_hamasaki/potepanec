module TaxonsHelper
  # 当該カテゴリに色付きの Variant をもつ商品が存在したら、「色から探す」を表示
  def display_color_search?
    return if @colors.nil?
    @colors.map { |color| color.has_product_with?(@taxon) }.include?(true)
  end
end