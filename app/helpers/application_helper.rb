module ApplicationHelper
  # ページタイトルを返す
  def full_title(page_title = '')
    base_title = 'BIGBAG Store'
    if page_title.empty?
      base_title
    else
      page_title + ' - ' + base_title
    end
  end

  # 文字列から属性名を取得
  def attr_name(str)
    str.downcase.gsub(' ', '-')
  end
end
