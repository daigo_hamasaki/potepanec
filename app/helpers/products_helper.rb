module ProductsHelper
  # ヴァリアントのセレクトボックス用の配列を返す
  def variants_array(product)
    # 配列先頭に、初期表示用のアイテムを挿入
    variants = [['Variants', '0']]
    # ヴァリアントごとに [名前, ID] の配列を追加
    product.variants.each do |variant|
      variants.push([variant.options_text, variant.id])
    end
    variants
  end
end