class Potepan::TaxonsController < ApplicationController
  def show
    @taxon = Spree::Taxon.includes(:products).find(params[:id])
    @taxonomies = Spree::Taxonomy.all

    @type     = params[:type] == 'list' ? 'list' : 'grid'
    @color_id = params[:color]
    # TODO: 課題8で @size_id をここに追加

    @products = @taxon.self_and_descendant_products.filtered_with_option_value(@color_id)
    # TODO: 課題8で @size_id 絞り込みをここに追加

    # サイドメニューの色リスト
    @colors = Spree::OptionType.get_option_values_with_presentation('Color')
  end
end