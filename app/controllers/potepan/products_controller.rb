class Potepan::ProductsController < ApplicationController
  MAX_PRODUCT_PROPERTIES = 3
  MAX_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.includes(:properties, :variants).find(params[:id])
    @related_products = @product.related_products(MAX_RELATED_PRODUCTS)

    @color_id = params[:color]
  end
end