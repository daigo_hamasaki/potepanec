class Potepan::TopController < ApplicationController
  MAX_NEW_PRODUCTS = 8

  def index
    @new_products = Spree::Product.order(available_on: :desc).limit(MAX_NEW_PRODUCTS)
  end
end