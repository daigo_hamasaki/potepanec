Spree::OptionValue.class_eval do
  # 引数の Taxon に属し、かつ選択中の OptionValue に属する商品の数をカウント
  def num_of_products(taxon)
    product_ids_with_option_value(taxon).uniq.count
  end

  def product_ids_with_option_value(taxon)
    variants.pluck(:product_id) & taxon.self_and_descendant_products.ids
  end

  # 引数の Taxon に、選択中の OptionValue の Variant をもつ商品があるか判定
  def has_product_with?(taxon)
    num_of_products(taxon) > 0
  end
end