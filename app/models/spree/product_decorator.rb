Spree::Product.class_eval do
  def related_products(max_number)
    # 商品と同じtaxonに所属する商品リスト
    related_products = taxons.leaves.includes(:products).map(&:products).flatten.compact.uniq
    # 自分自身を除外
    related_products.delete(self)
    related_products.sample(max_number)
  end

  # 特定の OptionValue を Valiant にもつ商品を抽出
  def self.filtered_with_option_value(option_value_id)
    return all unless option_value_id
    joins(variants: :option_values).where(spree_option_values_variants: { option_value_id: option_value_id }).uniq
  end

  # 特定の OptionValue に所属する Variant をひとつ取得
  def self_or_variant_with_option_value(option_value_id = nil)
    return self unless option_value_id
    variants.joins(:option_values_variants).find_by(spree_option_values_variants: { option_value_id: option_value_id })
  end
end