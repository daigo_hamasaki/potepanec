Spree::Taxon.class_eval do
  # 自身と子孫カテゴリーの商品の集合を返す
  def self_and_descendant_products
    Spree::Product.includes(:taxons).where(spree_taxons: {id: self_and_descendants})
  end
end