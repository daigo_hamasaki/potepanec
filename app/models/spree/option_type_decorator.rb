Spree::OptionType.class_eval do
  def self.get_option_values_with_presentation(presentation)
    option_type = includes(:option_values).find_by(presentation: presentation)
    option_type&.option_values || []
  end
end