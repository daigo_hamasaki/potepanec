require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#related_products" do
    context 'of the taxon which contains other products' do
      let(:taxon) { create(:taxon) }
      let(:products) do
        create_list(:product, 10) do |product|
          product.taxons << taxon
        end
      end

      let(:another_taxon) { create(:taxon) }
      let(:products_of_another_taxon) do
        create_list(:product, 10) do |product|
          product.taxons << another_taxon
        end
      end

      let(:max_related_products) { 4 }
      let(:product) { products.first }
      let(:related_products) { product.related_products(max_related_products) }

      it 'is not empty' do
        expect(related_products).not_to be_empty
      end

      it 'does not contain product itself' do
        expect(related_products).to_not include product
      end

      it 'does not contain other taxons' do
        products_of_another_taxon.each do |product_of_antoher_taxon|
          expect(related_products).to_not include product_of_antoher_taxon
        end
      end

      it 'has less than or equal to max_number of related_products' do
        expect(related_products.count).to be <= max_related_products
      end
    end

    context 'of the taxon which contains only the product itself' do
      let(:taxon) { create(:taxon) }
      let(:product) do
        create(:product) do |product|
          product.taxons << taxon
        end
      end

      let(:max_related_products) { 4 }
      let(:related_products) { product.related_products(max_related_products) }

      it 'should return empty array' do
        expect(related_products).to eq []
      end
    end
  end
end