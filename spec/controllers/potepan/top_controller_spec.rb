require 'rails_helper'

RSpec.describe Potepan::TopController, type: :controller do
  describe 'GET #index' do
    let(:max_new_products_counts) { Potepan::TopController::MAX_NEW_PRODUCTS }

    context 'when more than the MAX_NEW_PRODUCTS number of products exist' do
      let!(:products) { create_list(:new_product, max_new_products_counts * 2) }

      before do
        get :index
      end

      it 'returns http success' do
        expect(response).to have_http_status :success
      end

      it 'should be potepan_root_path ' do
        expect(request.path_info).to eq potepan_root_path
      end

      it 'should render index template' do
        expect(response).to render_template :index
      end

      it 'should assign @new_products' do
        expect(assigns(:new_products).to_a).to eq(products[0...max_new_products_counts])
      end

      it 'should have the newest product at the first of @new_products' do
        expect(assigns(:new_products).first).to eq(products[0])
      end

      it 'should have the least new product at the last of @new_products' do
        expect(assigns(:new_products).last).to eq(products[max_new_products_counts - 1])
      end

      it 'should have the MAX_NEW_PRODUCTS number of items in @new_products' do
        expect(assigns(:new_products).count).to eq max_new_products_counts
      end
    end

    context 'when only the half of the MAX_NEW_PRODUCTS number of products exist' do
      let(:half_of_the_max_new_products_counts) { (max_new_products_counts / 2).to_i }
      let!(:products) { create_list(:new_product, half_of_the_max_new_products_counts) }

      before do
        get :index
      end

      it 'should have just the same number of items in @new_products' do
        expect(assigns(:new_products).count).to eq half_of_the_max_new_products_counts
      end
    end
  end
end