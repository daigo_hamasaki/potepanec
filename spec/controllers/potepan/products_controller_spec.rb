require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it 'returns http success' do
      expect(response).to have_http_status :success
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq(product)
    end

    it 'should be potepan_product_path' do
      expect(request.path_info).to eq potepan_product_path(product.id)
    end

    it 'should render show template' do
      expect(response).to render_template :show
    end
  end
end
