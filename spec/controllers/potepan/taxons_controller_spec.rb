require 'rails_helper'

RSpec.describe Potepan::TaxonsController, type: :controller do
  describe 'GET #show' do
    let!(:option_color_value) { create(:option_color_value) }
    let!(:variant_with_color) { create(:variant, option_values: [option_color_value]) }
    let!(:variant_without_color) { create(:variant, product: variant_with_color.product) }
    let!(:product_with_color) { create(:product, variants: [variant_with_color]) }
    let!(:product_without_color) { create(:product, variants: [variant_without_color]) }
    let!(:taxon) { create(:taxon, products: [product_with_color, product_without_color]) }

    context 'with no additional params' do
      before do
        get :show, params: { id: taxon.id }
      end

      it 'returns http success' do
        expect(response).to have_http_status :success
      end

      it 'assigns @taxon' do
        expect(assigns(:taxon)).to eq(taxon)
      end

      it 'should be potepan_category_path' do
        expect(request.path_info).to eq potepan_category_path(taxon.id)
      end

      it 'should render show template' do
        expect(response).to render_template :show
      end
    end

    context 'with selected color param' do
      let!(:color) { option_color_value.presentation }

      before do
        color_id = Spree::OptionValue.find_by(presentation: color).id
        get :show, params: { id: taxon.id, color: color_id }
      end

      it 'should have products with colored variants' do
        expect(assigns(:products)).to include(product_with_color)
      end

      it 'should not have products without colored variants' do
        expect(assigns(:products)).not_to include(product_without_color)
      end
    end
  end
end
