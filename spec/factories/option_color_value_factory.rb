FactoryGirl.define do
  factory :option_color_value, class: 'Spree::OptionValue' do
    name 'Red'
    presentation 'Red'
    option_type { create(:option_color_type) }
  end
end
