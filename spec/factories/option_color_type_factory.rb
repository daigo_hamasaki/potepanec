FactoryGirl.define do
  factory :option_color_type, class: 'Spree::OptionType' do
    name 'foo-color'
    presentation 'Color'
  end
end
