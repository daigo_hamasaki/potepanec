require 'spree/testing_support/factories/product_factory'

FactoryGirl.define do
  # top_controller の新着商品テスト用に available_on を時系列で取得
  factory :new_product, parent: :product do
    sequence(:available_on) { |n| n.days.ago }
  end
end
